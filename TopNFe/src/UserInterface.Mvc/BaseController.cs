﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;
using TopDown.TopFramework.BusinessRules;
using TopDown.TopFramework.Common;
using TopDown.TopFramework.Common.Extensions;
using TopDown.TopFramework.Entities;
using TopDown.TopFramework.BusinessRules.Security.Authorization;

namespace TopDown.TopFramework.WebUserInterfaceHelper.Mvc
{
    /// <summary>
    /// Controlador base, independente da entidade vinculada. 
    /// </summary>
    /// <remarks>
    /// Utilizado para controle de erros.
    /// </remarks>
    public abstract class BaseController : Controller
    {
        /// <summary>
        /// Intercepta erros não tratados.
        /// </summary>
        /// <param name="filterContext">Contexto da execução.</param>
        protected override void OnException(ExceptionContext filterContext)
        {
            ILog logger = LogManager.GetLogger("GeneralLog");
            logger.Error(filterContext.Exception.Message);
            logger.Error(filterContext.Exception.StackTrace);

            //se a chamada do método foi com Ajax, devolve Json
            //senão redireciona para página de erro padrão
            if (filterContext.HttpContext.Request.IsAjaxRequest() && filterContext.Exception != null)
            {
                filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                filterContext.Result = new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                    {
                        filterContext.Exception.Message,
                        filterContext.Exception.StackTrace
                    }
                };
                filterContext.ExceptionHandled = true;
            }
            else
            {
                ViewDataDictionary viewData = new ViewDataDictionary();
                viewData.Add("msgErro", filterContext.Exception.Message);
                filterContext.Result = new ViewResult { MasterName = "Common", ViewName = "Erro", ViewData = viewData };
                filterContext.ExceptionHandled = true;
            }

        }
    }

    /// <summary>
    /// Controlador base, utilizado por entidade fortemente tipadas, onde se conhece o tipo do identificador da entidade. 
    /// </summary>
    public abstract class BaseController<T, K> : BaseController
        where T : class, IEntity, new()
    {
        /// <summary>
        /// Ação padrão da rota.
        /// </summary>
        public virtual ActionResult Index()
        {
            return RedirectToAction("List");
        }

        /// <summary>
        /// Método que ocorre antes da exibição da View.
        /// </summary>
        /// <param name="entity">Entidade que será enviada para a View.</param>
        protected virtual void FillExtraData(T entity)
        {
        }

        /// <summary>
        /// Ação utilizada para visualizar um formulário em branco.
        /// </summary>
        [Authorization(PermissionType.Inclusao)]
        public virtual ActionResult New()
        {
            // Entidade nova, com valores padrão
            T entity = new T();

            // Preenche o ViewData com dados extras (normalmente usado em DropDownLists)
            FillExtraData(entity);

            return View(entity);
        }

        /// <summary>
        /// Ação utilizada para visualizar um formulário em branco.
        /// </summary>
        /// <param name="entity">Formulário preenchido.</param>
        [HttpPost]
        [Authorization(PermissionType.Inclusao)]
        public virtual ActionResult New(T entity)
        {
            // Caso o modelo não esteja válido, exibe o formulário para correções
            if (!ModelState.IsValid)
            {
                FillExtraData(entity);
                return View(entity);
            }

            // Efetua a persistência usando a classe genérica de negócios
            IRules<T, K> rules = RulesManager.CreateByEntity<T, K>();
            rules.Create(entity);

            return RedirectToAction("List");
        }

        /// <summary>
        /// Ação utilizada para visualizar um formulário de edição.
        /// </summary>
        /// <param name="id">Identificador da entidade desejada.</param>
        [Authorization(PermissionType.Consulta)]
        public virtual ActionResult Edit(K id)
        {
            // Obtém os dados da entidade usando a classe genérica de negócios
            IRules<T, K> rules = RulesManager.CreateByEntity<T, K>();
            T entity = rules.GetById(id);

            // Preenche o ViewData com dados extras (normalmente usado em DropDownLists)
            FillExtraData(entity);

            return View(entity);
        }

        /// <summary>
        /// Ação utilizada para persistir um objeto atualizado.
        /// </summary>
        /// <param name="entity">Entidade atualizada enviada pelo formulário.</param>
        /// <returns></returns>
        [HttpPost]
        [Authorization(PermissionType.Atualizacao)]
        public virtual ActionResult Edit(T entity)
        {
            // Caso o modelo não esteja válido, exibe o formulário para correções
            if (!ModelState.IsValid)
                return View();

            // Efetua a persistência usando a classe genérica de negócios
            IRules<T, K> rules = RulesManager.CreateByEntity<T, K>(false);
            rules.Update(entity);

            return RedirectToAction("List");
        }

        /// <summary>
        /// Ação utilizada para listar os objetos.
        /// </summary>
        [Authorization(PermissionType.Consulta)]
        public virtual ActionResult List()
        {
            return View();
        }

        /// <summary>
        /// Ação utilizada pelo jqGrid para carregar os dados.
        /// </summary>
        [HttpPost]
        [Authorization(PermissionType.Consulta)]
        public virtual JsonResult GetPagedInJson(int page, int rows, string sidx, string sord)
        {
            int totalFound = 0;
            IRules<T> rules = RulesManager.CreateByEntity<T>();
            Expression<Func<T, bool>> filtro = null;
            if (!string.IsNullOrEmpty(Request["_search"]) && bool.Parse(Request["_search"]))
            {
                string searchField = Request["searchField"];
                string searchString = Request["searchString"];
                string searchOper = Request["searchOper"];

                filtro = CreateExpression(searchField, searchString, searchOper);
            }
            IEnumerable<T> list = rules.FindAll(filtro, sidx, sord.ToLower() == "desc", page, rows, out totalFound);

            return new JsonResult
            {
                Data = new
                {
                    Items = list.ToArray(),
                    ItemCount = totalFound,
                    PageIndex = page,
                    TotalPages = (totalFound / rows) + 1
                }
            };
        }

        /// <summary>
        /// Ação utilizada pelo jqGrid para carregar os dados.
        /// </summary>
        [HttpPost]
        [Authorization(PermissionType.Consulta)]
        public virtual JsonResult GetAllInJson(string sidx)
        {
            IRules<T> rules = RulesManager.CreateByEntity<T>();
            IEnumerable<T> list = rules.FindAll(null, sidx, false);

            return new JsonResult
            {
                Data = new
                {
                    Items = list.ToArray(),
                    ItemCount = list.Count(),
                    PageIndex = 1,
                    TotalPages = 1
                }
            };
        }

        /// <summary>
        /// Ação utilizada para excluir uma entidade.
        /// </summary>
        /// <param name="id">Identificador da entidade que se deseja excluir.</param>
        [HttpPost]
        [Authorization(PermissionType.Exclusao)]
        public virtual JsonResult DeleteByIdInJson(K id)
        {
            try
            {
                IRules<T, K> rules = RulesManager.CreateByEntity<T, K>();
                rules.DeleteById(id);

                return new JsonResult
                {
                    Data = new
                    {
                        Message = "Exclusão realizada com sucesso!",
                        MessageType = "Success"
                    }
                };
            }
            catch (Exception ex)
            {
                ILog logger = LogManager.GetLogger("GeneralLog");
                logger.Warn("Não foi possível excluir os dados.");
                logger.Error(ex.Message);

                return new JsonResult
                {
                    Data = new
                    {
                        Message = "Não foi possível excluir os dados.",
                        MessageType = "Error"
                    }
                };
            }
        }

        /// <summary>
        /// Cria uma expressão lambda a parti dos argumentos fornecidos pelo jqGrid.
        /// </summary>
        /// <param name="searchField">Campo que será buscado.</param>
        /// <param name="searchString">Valor da busca.</param>
        /// <param name="searchOper">Operador da busca.</param>
        /// <returns>Retorna a expressão lambda equivalente.</returns>
        public virtual Expression<Func<T, bool>> CreateExpression(string searchField, string searchString, string searchOper)
        {
            Expression exp = null;
            var p = Expression.Parameter(typeof(T), "p");

            try
            {
                Expression propertyAccess = searchField.ToExpression(p);

                switch (searchOper)
                {
                    case "bw":
                        exp = Expression.Call(propertyAccess, typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) }), Expression.Constant(searchString));
                        break;
                    case "cn":
                        exp = Expression.Call(propertyAccess, typeof(string).GetMethod("Contains", new Type[] { typeof(string) }), Expression.Constant(searchString));
                        break;
                    case "ew":
                        exp = Expression.Call(propertyAccess, typeof(string).GetMethod("EndsWith", new Type[] { typeof(string) }), Expression.Constant(searchString));
                        break;
                    case "gt":
                        exp = Expression.GreaterThan(propertyAccess, Expression.Constant(searchString, propertyAccess.Type));
                        break;
                    case "ge":
                        exp = Expression.GreaterThanOrEqual(propertyAccess, Expression.Constant(searchString, propertyAccess.Type));
                        break;
                    case "lt":
                        exp = Expression.LessThan(propertyAccess, Expression.Constant(searchString, propertyAccess.Type));
                        break;
                    case "le":
                        exp = Expression.LessThanOrEqual(propertyAccess, Expression.Constant(searchString, propertyAccess.Type));
                        break;
                    case "eq":
                        exp = Expression.Equal(propertyAccess, Expression.Constant(searchString.ToType(propertyAccess.Type), propertyAccess.Type));
                        break;
                    case "ne":
                        exp = Expression.NotEqual(propertyAccess, Expression.Constant(searchString, propertyAccess.Type));
                        break;
                    default:
                        return null;
                }

                return (Expression<Func<T, bool>>)Expression.Lambda(exp, p);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Ação padrão para erro
        /// </summary>
        public virtual ActionResult Erro()
        {
            return View();
        }

    }
}
