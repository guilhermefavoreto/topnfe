﻿using System;
using System.Linq.Expressions;

namespace TopDown.TopFramework.Common.Extensions
{
    /// <summary>
    /// Classe contendo extension methods para objetos String.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Converte um objeto String para o tipo especificado.
        /// </summary>
        /// <param name="origin">Objeto string original</param>
        /// <param name="dest">Tipo desejado.</param>
        /// <returns>O objeto convertido para o tipo desejado.</returns>
        public static object ToType(this string origin, Type dest)
        {
            if (dest == typeof(decimal?) || dest == typeof(decimal))
                return decimal.Parse(origin);

            if (dest == typeof(int?) || dest == typeof(int))
                return int.Parse(origin);
            
            return origin;
        }

        /// <summary>
        /// Converte um objeto String para um objeto MemberExpression.
        /// </summary>
        /// <param name="origin">Objeto string original</param>
        /// <param name="p">Parâmetro de entrada do objeto Expression.</param>
        /// <returns>Uma expressão referente ao membro </returns>
        public static Expression ToExpression(this string origin, ParameterExpression p)
        {
            string[] properties = origin.Split('.');

            Type propertyType = p.Type;
            Expression propertyAccess = p;

            foreach (var prop in properties)
            {
                var property = propertyType.GetProperty(prop);
                propertyType = property.PropertyType;
                propertyAccess = Expression.MakeMemberAccess(propertyAccess, property);
            }

            return propertyAccess;
        }
    }
}
