﻿<%@ Import Namespace="TopDown.TopNFe.Entities" %>
<%@ Page Language="C#" Inherits="ViewPage<NFeEntity>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Cadastro de Notas Fiscais</title>
    <link href="/Content/Styles/general.css" 
          rel="stylesheet" type="text/css" />
    <link href="/Content/Styles/forms.css" 
          rel="stylesheet" type="text/css" />
    <link href="/Content/Styles/jquery-ui-1.7.2.custom.css" 
          rel="stylesheet" type="text/css" />
    <link href="/Content/Styles/ui.jqgrid.css" 
          rel="stylesheet" type="text/css" />
    <link href="/Content/Styles/uploadify.css" 
          rel="stylesheet" type="text/css" />
    <script src="/Scripts/jquery-1.3.2.min.js"
            type="text/javascript"></script>
    <script src="/Scripts/jquery.validate.min.js"
            type="text/javascript"></script>
    <script src="/Scripts/MicrosoftMvcJQueryValidation.js"
            type="text/javascript"></script>
    <script src="/Scripts/jquery-ui-1.7.2.custom.min.js"
            type="text/javascript"></script>
    <script src="/Scripts/grid.locale-pt-br.js"
            type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js"
            type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid-default.js"
            type="text/javascript"></script>
    <script src="/Scripts/swfobject.js" 
            type="text/javascript"></script>
    <script src="/Scripts/jquery.uploadify.v2.1.0.min.js" 
            type="text/javascript"></script>
    <script src="/Scripts/jquery.jqDropDownList-1.0.js" 
            type="text/javascript"></script>
</head>
<body>
<%
    Html.EnableClientValidation();
    using (Html.BeginForm())
    { %>
    <div class="cabecalho-formulario">
        <h2>Cadastro de Notas Fiscais</h2>
    </div>
<%  
        Html.RenderPartial("FormControl"); 
    } %>
</body>
</html>
