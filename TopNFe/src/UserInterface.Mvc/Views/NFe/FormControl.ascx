﻿<%@ Import Namespace="System.Web.Mvc" %>
<%@ Import Namespace="TopDown.TopNFe.Entities" %>
<%@ Control Language="C#" Inherits="ViewUserControl<NFeEntity>" %>
<%--
<link href="/Content/Styles/general.css" rel="stylesheet" type="text/css" />
<link href="/Content/Styles/forms.css" rel="stylesheet" type="text/css" />
<link href="/Content/Styles/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
<link href="/Content/Styles/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<link href="/Content/Styles/uploadify.css" rel="stylesheet" type="text/css" />
<script src="/Scripts/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
<script src="/Scripts/grid.locale-pt-br.js" type="text/javascript"></script>
<script src="/Scripts/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="/Scripts/jquery.jqGrid-default.js" type="text/javascript"></script>
<script src="/Scripts/swfobject.js" type="text/javascript"></script>
<script src="/Scripts/jquery.uploadify.v2.1.0.min.js" type="text/javascript"></script>
--%>

<script type="text/javascript">
    $(function() {
        $("#uploader").uploadify({
            uploader: "/Content/uploadify.swf",
            script: '/NFe/Upload',
            cancelImg: "/Content/Images/cancel.png",
            fileDesc: "Notas Fiscais Eletrônicas (NFe's)",
            fileExt: "*.xml",
            buttonText: 'Escolher arquivo',
            auto: true,
            multi: false,
            onComplete: function(event, queueID, fileObj, response, data) {
                var returnedData = JSON.parse(response);
                if (returnedData.IsValid) {
                    alert("A nota fiscal foi salva com sucesso.");
                    $("#grid").trigger("reloadGrid");
                }
                else {
                    alert("A nota fiscal é inválida.");
                }
            }
        });
        $("#grid").jqGrid({
            url: '<%= Url.Action("GetPagedInJson") %>',
            colNames: [
                'Número',
                'Data'
            ],
            colModel: [
                { name: 'Numero', width: '120' },
                { name: 'DataUploadFormatada', width: '120' }
            ],
            sortname: 'DataUpload'

        }).navGrid(
            "#pager",
            {
                add: false,
                edit: false,
                del: false,
                view: false
            }
        );
        $("#Empresa_Id").change(function () {
            var actionUrl = '<%= Url.Action("GetPagedInJson") %>';
            actionUrl += '/?_search=true';
            actionUrl += '&searchField=Empresa.Id';
            actionUrl += '&searchOper=eq';
            actionUrl += '&searchString=' + $(this).val();
            $("#grid").setGridParam({ url: actionUrl });
            $("#grid").trigger("reloadGrid");
            $("#uploader").uploadifySettings('scriptData',
                { 'empresaId': $(this).val() });
        });
    });
</script>
<div class="formulario">
    <div class="item-form inteiro">
        <span class="titulo-item">
            <%= Html.LabelFor(model => model.Empresa)%>
        </span>
        <div class="controle-item">
            <%= Html.EditorFor(model => model.Empresa)%>
        </div>
    </div>
    <div class="item-form inteiro">
        <span class="titulo-item">
            <label for="uploader">
                Upload da nota fiscal</label>
        </span>
        <div class="controle-item">
            <input type="file" id="uploader" />
        </div>
    </div>
    <div class="item-form inteiro">
        <span class="titulo-item">
            <label for="uploader">
                Notas fiscais armazenadas</label>
        </span>
        <div class="controle-item">
            <table id="grid">
            </table>
            <div id="pager">
            </div>
        </div>
    </div>
</div>

