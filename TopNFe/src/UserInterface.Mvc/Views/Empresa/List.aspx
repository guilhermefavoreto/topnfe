﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Lista de Empresas</title>
    <link href="/Content/Styles/jquery-ui-1.7.2.custom.css" 
          rel="stylesheet" type="text/css" />
    <link href="/Content/Styles/ui.jqgrid.css" 
          rel="stylesheet" type="text/css" />
    <script src="/Scripts/jquery-1.3.2.min.js"
            type="text/javascript"></script>
    <script src="/Scripts/jquery-ui-1.7.2.custom.min.js"
            type="text/javascript"></script>
    <script src="/Scripts/grid.locale-pt-br.js"
            type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid.min.js"
            type="text/javascript"></script>
    <script src="/Scripts/jquery.jqGrid-default.js"
            type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#grid").jqGrid({ //Funcionalidades comuns de um grid e também ordena, filtra, redimensiona colunas, possui toolbar etc.
                url: '<%= Url.Action("GetPagedInJson") %>',
                colNames: [
                'Razão Social',
                'CNPJ'
            ],
                colModel: [
                { name: 'RazaoSocial', width: '600' },
                { name: 'Cnpj', width: '120' }
            ],
                sortname: 'RazaoSocial'
            }).navGrid(
            "#pager",
            {
                editfunc: function (selrow) {
                    window.location = './Edit/' + selrow;
                },
                addfunc: function (selrow) {
                    window.location = './New';
                }
            }
        );
        });
    </script>
</head>
<body>
    <div class="cabecalho-formulario">
        <h2>Lista de Empresas</h2>
    </div> 
    <table id="grid"></table>
    <div id="pager"></div>
</body>
</html>

