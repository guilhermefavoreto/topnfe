﻿<%@ Import Namespace="TopDown.TopNFe.Entities" %>
<%@ Page Language="C#" Inherits="ViewPage<EmpresaEntity>" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Cadastro de Empresas</title>
    <link href="/Content/Styles/general.css" rel="stylesheet" type="text/css" />
    <link href="/Content/Styles/forms.css" rel="stylesheet" type="text/css" />
    <script src="/Scripts/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="/Scripts/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/Scripts/MicrosoftMvcJQueryValidation.js" type="text/javascript"></script>
</head>
<body>
<%
    Html.EnableClientValidation();
    using (Html.BeginForm())
    { %>
    <div class="cabecalho-formulario">
        <h2>
            Cadastro de Empresas
        </h2>
    </div>
    <div class="barra-formulario">
        <input type="submit" name="salvar" value="Salvar" />
        <button id="cancelar"
                onclick="window.self.location='/Empresa'; return false;">
            Cancelar        
        </button>
    </div>    
<%  
        Html.RenderPartial("FormControl");
    } %>
</body>
</html>
