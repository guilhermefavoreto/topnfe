﻿<%@ Import Namespace="System.Web.Mvc" %>
<%@ Import Namespace="TopDown.TopNFe.Entities" %>
<%@ Control Language="C#" Inherits="ViewUserControl<EmpresaEntity>" %>
<%--
<link href="/Content/Styles/general.css" rel="stylesheet" type="text/css" />
<link href="/Content/Styles/forms.css" rel="stylesheet" type="text/css" />

Note que as referências aos arquivos de CSS foram comentados no componentes. 
Isso ocorre porque é melhor que eles estejam referenciados na view principal.
O mesmo vale para arquivos de script (.js). Ao mesmo tempo o comentário é 
importante para deixar registrado quais referências são necessárias para 
o perfeito funcionamento do componente.
--%>
<div class="formulario">
    <%= Html.HiddenFor(model => model.Id) %>
    <div class="item-form inteiro">
        <span class="titulo-item">
            <%= Html.LabelFor(model => model.RazaoSocial)%>
        </span>
        <div class="controle-item">
            <%= Html.EditorFor(model => model.RazaoSocial)%>
        </div>
        <div>
            <%= Html.ValidationMessageFor(model => model.RazaoSocial) %>
        </div>
    </div>
    <div class="item-form quarto">
        <span class="titulo-item">
            <%= Html.LabelFor(model => model.Cnpj)%>
        </span>
        <div class="controle-item">
            <%= Html.EditorFor(model => model.Cnpj)%>
        </div>
        <div>
            <%= Html.ValidationMessageFor(model => model.Cnpj)%>
        </div>
    </div>
</div>
