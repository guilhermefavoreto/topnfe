﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TopDown.TopFramework.WebUserInterfaceHelper.Mvc;
using TopDown.TopNFe.Entities;
using System.Web.Mvc;
using TopDown.TopNFe.BusinessRules;


namespace UserInterface.Mvc.Controller
{
    public class NFeController : BaseController<NFeEntity, long>
    {
        [HttpPost]
        public JsonResult Upload(HttpPostedFileBase fileData)
        {
            try
            {
                int empresaId = int.Parse(Request["empresaId"]);
                NFeEntity nfe;
                NFeRules rules = new NFeRules();
                if (rules.Parse(fileData.InputStream, out nfe))
                {
                    EmpresaRules empresaRules = new EmpresaRules();
                    nfe.Empresa = empresaRules.Load(empresaId);
                    rules.Create(nfe);

                    return new JsonResult
                    {
                        Data = new { IsValid = true }
                    };
                }
            }
            catch
            {
            }

            return new JsonResult
            {
                Data = new { IsValid = false }
            };
        }

        public NFeController()
            : base()
        {
        }
    }
}