﻿using System;
using TopDown.TopFramework.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Web.Extensions;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace TopDown.TopNFe.Entities
{
    public partial class EmpresaEntity : BaseEntity
    {

        [DisplayName("Notas fiscais")]
        [ScriptIgnore]
        public virtual IList<NFeEntity> NotasFiscais { get; set; }
        public EmpresaEntity()
            : base()
        {
        }
    }
}