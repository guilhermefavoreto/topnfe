﻿using System;
using TopDown.TopFramework.Entities;

namespace TopDown.TopNFe.Entities
{
    public partial class NFeEntity : BaseEntity
    {
        public NFeEntity()
            : base()
        { 
        }
        public virtual string DataUploadFormatted
        {
            get
            {
                return DataUpload.ToString("g");
            }
        }
    }
}