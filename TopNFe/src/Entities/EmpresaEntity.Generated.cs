﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TopDown.TopFramework.Entities;

namespace TopDown.TopNFe.Entities
{
    public partial class EmpresaEntity : BaseEntity
    {
        #region " Identificador da classe "

        public virtual int Id { get; set; }

        #endregion

        #region " Propriedades publicas da classe "

        [Required(ErrorMessage = "O campo 'RazaoSocial' ? obrigat?rio.")]
        [StringLength(120, ErrorMessage = "O campo 'RazaoSocial' suporta no m?ximo 120 caracteres.")]
        [DisplayName("RazaoSocial")]
        public virtual string RazaoSocial { get; set; }
        
        [Required(ErrorMessage = "O campo 'Cnpj' ? obrigat?rio.")]
        [StringLength(20, ErrorMessage = "O campo 'Cnpj' suporta no m?ximo 20 caracteres.")]
        [DisplayName("Cnpj")]
        public virtual string Cnpj { get; set; }
        
        #endregion
    }
}