﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TopDown.TopFramework.Entities;

namespace TopDown.TopNFe.Entities
{
    public partial class NFeEntity : BaseEntity
    {
        #region " Identificador da classe "

        public virtual long Id { get; set; }

        #endregion

        #region " Propriedades publicas da classe "

        [Required(ErrorMessage = "O campo 'Numero' ? obrigat?rio.")]
        [DisplayName("Numero")]
        public virtual int Numero { get; set; }
        
        [Required(ErrorMessage = "O campo 'Versao' ? obrigat?rio.")]
        [StringLength(10, ErrorMessage = "O campo 'Versao' suporta no m?ximo 10 caracteres.")]
        [DisplayName("Versao")]
        public virtual string Versao { get; set; }
        
        [Required(ErrorMessage = "O campo 'Xml' ? obrigat?rio.")]
        [DisplayName("Xml")]
        public virtual string Xml { get; set; }
        
        [Required(ErrorMessage = "O campo 'DataUpload' ? obrigat?rio.")]
        [DisplayName("DataUpload")]
        public virtual DateTime DataUpload { get; set; }
        
        #endregion

        #region " Relacionamentos "

        public virtual EmpresaEntity Empresa { get; set; }

        #endregion
    }
}