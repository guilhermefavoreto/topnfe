﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TopDown.TopFramework.BusinessRules;
using TopDown.TopNFe.Entities;
using System.IO;
using System.Xml.XPath;
using System.Xml;

namespace TopDown.TopNFe.BusinessRules
{
    public class NFeRules :BaseRules<NFeEntity, int>
    {
        private bool ValidateSchema(Stream uploadedFile, out XPathNavigator xpathNavigator)
        {
            try
            {
                string targetNamespace = "http://www.portalfiscal.inf.br/nfe";
                string schemaPath = Path.Combine(
                                        AppDomain.CurrentDomain.RelativeSearchPath, "nfe_v1.10.xsd");
                XmlReaderSettings readerSettings = new XmlReaderSettings();
                readerSettings.ValidationType = ValidationType.Schema;
                readerSettings.Schemas.Add(targetNamespace, schemaPath);
                XmlReader reader = XmlReader.Create(uploadedFile, readerSettings, targetNamespace);
                xpathNavigator = new XPathDocument(reader).CreateNavigator();
                return true;
            }
            catch
            {
                xpathNavigator = null;
                return false;
            }
        }

        private void Hydrate(out NFeEntity nfe, XPathNavigator xpathNavigator)
        {
            XmlNamespaceManager manager =
                new XmlNamespaceManager(xpathNavigator.NameTable);
            manager.AddNamespace("nfe", "http://www.portalfiscal.inf.br/nfe");
            nfe = new NFeEntity();
            nfe.Xml = xpathNavigator.OuterXml;
            nfe.Versao = GetVersao(xpathNavigator, manager);
            nfe.Numero = GetNumero(xpathNavigator, manager);
            nfe.DataUpload = DateTime.Now;
        }

        private int GetNumero(XPathNavigator xpathNavigator,
                        XmlNamespaceManager manager)
        {
            XPathNodeIterator iterator =
                xpathNavigator.Select(@"/nfe:NFe/nfe:infNFe/nfe:ide/nfe:nNF", manager);
            iterator.MoveNext();
            return int.Parse(iterator.Current.Value);
        }

        private string GetVersao(XPathNavigator xpathNavigator, XmlNamespaceManager manager)
        {
            XPathNodeIterator iterator =
                xpathNavigator.Select(@"/nfe:NFe/nfe:infNFe/@versao", manager);
            iterator.MoveNext();
            return iterator.Current.Value;
        }

        public bool Parse(Stream uploadedFile, out NFeEntity nfe)
        {
            XPathNavigator xpathNavigator;
            if (ValidateSchema(uploadedFile, out xpathNavigator))
            {
                Hydrate(out nfe, xpathNavigator);
                return true;
            }
            nfe = null;
            return false;
        }
    }
}
