﻿$(function() {
    $.jgrid.defaults = $.extend($.jgrid.defaults, {
        datatype: "json",
        mtype: "POST",
        height: '100%',
        jsonReader: {
            repeatitems: false,
            id: "Id",
            root: "Items",
            records: "ItemCount",
            page: "PageIndex",
            total: "TotalPages"
        },
        editurl: './DeleteByIdInJson/',
        pginput: false,
        viewrecords: true,
        pager: "#pager"
    });
})